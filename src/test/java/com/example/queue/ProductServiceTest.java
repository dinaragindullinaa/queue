package com.example.queue;

import com.example.queue.models.Image;
import com.example.queue.models.Product;
import com.example.queue.repositories.ProductRepository;
import com.example.queue.repositories.QueueRepository;
import com.example.queue.services.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {
    @Mock
    private ProductRepository productRepository;
    @Mock
    private QueueRepository queueRepository;

    @InjectMocks
    private ProductService productService;

    @Test
    void successfulChangeProduct(){
        Product product = new Product(0L,"TMP","TMP_DESCRIPTION",1000,10,0,"TMP_ADDRESS", (Image) null,0L);
        Product product1 = productService.changeProductInfo(product,0L,"CHANGED_NAME","CHANGED_DESCRIPTION",2000,10);
        Assertions.assertEquals(product1.getName(),"CHANGED_NAME");
        Assertions.assertEquals(product1.getDescription(),"CHANGED_DESCRIPTION");
        Assertions.assertEquals(product1.getPrice(),2000);
        Assertions.assertEquals(product1.getNumber(),10);
    }

    @Test
    void noChangeProduct(){
        Product product = new Product(0L,"TMP","TMP_DESCRIPTION",1000,10,0,"TMP_ADDRESS", (Image) null,0L);
        Product product1 = productService.changeProductInfo(product,null,null,null,null,null);
        Assertions.assertEquals(product1.getName(),product.getName());
        Assertions.assertEquals(product1.getDescription(),product.getDescription());
        Assertions.assertEquals(product1.getPrice(),product.getPrice());
        Assertions.assertEquals(product1.getNumber(),product.getNumber());
    }

    @Test
    void incorrectPriceChangeProduct(){
        Product product = new Product(0L,"TMP","TMP_DESCRIPTION",1000,10,0,"TMP_ADDRESS", (Image) null,0L);
        Product product1 = productService.changeProductInfo(product,null,null,null,-1,null);
        Assertions.assertEquals(product1.getName(),product.getName());
        Assertions.assertEquals(product1.getDescription(),product.getDescription());
        Assertions.assertEquals(product1.getPrice(),product.getPrice());
        Assertions.assertEquals(product1.getNumber(),product.getNumber());
    }

    @Test
    void incorrectNumberOfProductChangeProduct(){
        Product product = new Product(0L,"TMP","TMP_DESCRIPTION",1000,10,0,"TMP_ADDRESS", (Image) null,0L);
        Product product1 = productService.changeProductInfo(product,null,null,null,null,-1);
        Assertions.assertEquals(product1.getName(),product.getName());
        Assertions.assertEquals(product1.getDescription(),product.getDescription());
        Assertions.assertEquals(product1.getPrice(),product.getPrice());
        Assertions.assertEquals(product1.getNumber(),product.getNumber());
    }

    @Test
    void successfulSaveProduct() throws IOException {
        MultipartFile file = mock(MultipartFile.class);
        Image image = new Image();
        when(file.getSize()).thenReturn(1L);
        Product product = new Product(0L,"TMP","TMP_DESCRIPTION",1000,10,0,"TMP_ADDRESS", image,0L);
        when(productRepository.save(any())).thenReturn(product);
        Product product1 = productService.saveProduct(product,file);

        Assertions.assertEquals("TMP",product1.getName());
        Assertions.assertEquals("TMP_DESCRIPTION", product1.getDescription());
        Assertions.assertEquals(1000, product1.getPrice());
        Assertions.assertEquals(10, product1.getNumber());
        Assertions.assertEquals(0, product1.getNumberOfPeople());
        Assertions.assertEquals("TMP_ADDRESS", product1.getAddress());
    }

    @Test
    void noSaveProduct() throws IOException {
        Product product1 = productService.saveProduct(null,null);
        Assertions.assertEquals(null,product1);
    }

    @Test
    void incorrectPriceSaveProduct() throws IOException {
        Product product = new Product(0L,"TMP","TMP_DESCRIPTION",-1000,10,0,"TMP_ADDRESS", (Image) null,0L);
        Product product1 = productService.saveProduct(product,null);
        Assertions.assertEquals(null,product1);
    }

    @Test
    void incorrectNumberOfProductSaveProduct() throws IOException {
        Product product = new Product(0L,"TMP","TMP_DESCRIPTION",1000,-10,0,"TMP_ADDRESS", (Image) null,0L);
        Product product1 = productService.saveProduct(product,null);
        Assertions.assertEquals(null,product1);
    }
}
