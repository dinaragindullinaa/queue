package com.example.queue;

import com.example.queue.models.Image;
import com.example.queue.models.Product;
import com.example.queue.models.Queue;
import com.example.queue.models.User;
import com.example.queue.repositories.ProductRepository;
import com.example.queue.repositories.QueueRepository;
import com.example.queue.repositories.UserRepository;
import com.example.queue.services.QueueService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class QueueServiceTest {
    @Mock
    private ProductRepository productRepository;
    @Mock
    private QueueRepository queueRepository;
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private QueueService queueService;

    @Test
    void successfulToGetInLine(){
        Product product = mock(Product.class);
        when(queueRepository.countByProductId(any())).thenReturn(10);
        when(productRepository.findByID(any())).thenReturn(product);
        when(product.getNumber()).thenReturn(50);
        when(product.getNumberOfPeople()).thenReturn(10);
        Queue queue = queueService.toGetInLine(0L,null);
        Assertions.assertEquals(10, queue.getPeopleAhead());
        Assertions.assertEquals(true, queue.getActive());
    }

    @Test
    void noActiveToGetInLine(){
        Product product = mock(Product.class);
        when(queueRepository.countByProductId(any())).thenReturn(10);
        when(productRepository.findByID(any())).thenReturn(product);
        when(product.getNumber()).thenReturn(5);
        when(product.getNumberOfPeople()).thenReturn(10);
        Queue queue = queueService.toGetInLine(0L,null);
        Assertions.assertEquals(10, queue.getPeopleAhead());
        Assertions.assertEquals(false, queue.getActive());
    }

    @Test
    void successfulTakeProduct(){
        Queue queue = mock(Queue.class);
        when(queueRepository.findByProductIdAndUserId(any(),any())).thenReturn(queue);
        Product product = new Product(0L,"TMP","TMP_DESCRIPTION",1000,10,5,"TMP_ADDRESS", (Image) null,0L);
        when(productRepository.findByID(any())).thenReturn(product);
        Product product1 = queueService.takeProduct(null, null);
        Assertions.assertEquals(9,product1.getNumber());
        Assertions.assertEquals(4,product1.getNumberOfPeople());
    }

    @Test
    void cantFindProductToTakeProduct(){
        Queue queue = mock(Queue.class);
        when(queueRepository.findByProductIdAndUserId(any(),any())).thenReturn(queue);
        Product product = null;
        when(productRepository.findByID(any())).thenReturn(product);
        NullPointerException thrown = Assertions.assertThrows(
                NullPointerException.class,
                () -> queueService.takeProduct(null,null)
        );
        Assertions.assertTrue(thrown.getMessage().contains("Продукт не найден"));
    }

    @Test
    void cantFindQueueToTakeProduct(){
        Queue queue = null;
        when(queueRepository.findByProductIdAndUserId(any(),any())).thenReturn(queue);
        NullPointerException thrown = Assertions.assertThrows(
                NullPointerException.class,
                () -> queueService.takeProduct(null,null)
        );
        Assertions.assertTrue(thrown.getMessage().contains("Очереди нет для пользователя и продукта"));
    }

    @Test
    void successfulDoNotTakeProduct(){
        Queue queue = mock(Queue.class);
        when(queueRepository.findByProductIdAndUserId(any(),any())).thenReturn(queue);
        Product product = new Product(0L,"TMP","TMP_DESCRIPTION",1000,10,5,"TMP_ADDRESS", (Image) null,0L);
        when(productRepository.findByID(any())).thenReturn(product);
        List<Queue> nextQueue = mock(List.class);
        when(queueRepository.findByProductIdAndActiveFalseOrderByDateOfCreated(null)).thenReturn(nextQueue);
        Product product1 = queueService.doNotTakeProduct(null, null);
        Assertions.assertEquals("TMP",product1.getName());
        Assertions.assertEquals(10,product1.getNumber());
        Assertions.assertEquals(4,product1.getNumberOfPeople());
    }

    @Test
    void cantFindProductToDoNotTakeProduct(){
        Queue queue = mock(Queue.class);
        when(queueRepository.findByProductIdAndUserId(any(),any())).thenReturn(queue);
        Product product = null;
        when(productRepository.findByID(any())).thenReturn(product);
        NullPointerException thrown = Assertions.assertThrows(
                NullPointerException.class,
                () -> queueService.doNotTakeProduct(null,null)
        );
        Assertions.assertTrue(thrown.getMessage().contains("Продукт не найден"));
    }

    @Test
    void successfulToGetOutLine(){
        User user = mock(User.class);
        Queue queue = mock(Queue.class);
        when(queue.getPeopleAhead()).thenReturn(5);
        when(queueRepository.findByProductIdAndUserId(any(),any())).thenReturn(queue);
        Product product = new Product(0L,"TMP","TMP_DESCRIPTION",1000,10,5,"TMP_ADDRESS", (Image) null,0L);
        when(productRepository.findByID(any())).thenReturn(product);
        Product product1 = queueService.toGetOutLine(null,null);
        verify(queueRepository,times(1)).findByProductIdOrderByDateOfCreated(any());
        Assertions.assertEquals(4, product1.getNumberOfPeople());

    }

    @Test
    void nullPointerExceptionToGetOutLine(){
        NullPointerException thrown = Assertions.assertThrows(
                NullPointerException.class,
                () -> queueService.toGetOutLine(null,null)
        );
        Assertions.assertTrue(thrown.getMessage().contains("null"));
    }
}
