package com.example.queue.services;

import com.example.queue.models.Product;
import com.example.queue.models.Queue;
import com.example.queue.models.User;
import com.example.queue.repositories.ProductRepository;
import com.example.queue.repositories.QueueRepository;
import com.example.queue.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class QueueService {
    private final ProductRepository productRepository;
    private final QueueRepository queueRepository;
    private final UserRepository userRepository;

    // становление в очередь
    public Queue toGetInLine(Long id, Principal principal){
        User user = getUserByPrincipal(principal);
        Queue queue = new Queue();
        queue.setProductId(id);
        queue.setUserId(user.getId());
        queue.setPeopleAhead(queueRepository.countByProductId(id));
        Product product = productRepository.findByID(id);
        if(product.getNumber()-product.getNumberOfPeople()>0){
            queue.setActive(true);
        }

        product.setNumberOfPeople(product.getNumberOfPeople()+1);
        productRepository.save(product);
        queueRepository.save(queue);
        return queue;
    }

    // вызывается после того, как пользователь дал согласие на получение товара
    public Product takeProduct(Principal principal, Long id){
        Queue queue = getQueueByTwoId(principal,id);
        if (queue==null){
            throw new NullPointerException("Очереди нет для пользователя и продукта");
        }
        queueRepository.delete(queue);
        Product product = productRepository.findByID(id);
        try {
            product.setNumber(product.getNumber()-1);
            product.setNumberOfPeople(product.getNumberOfPeople()-1);
            changePeopleAhead(id,0);
            productRepository.save(product);
        }
        catch (Exception e){
            throw new NullPointerException("Продукт не найден");
        }
        return product;
    }

    // отказ от получения товара
    public Product doNotTakeProduct(Principal principal, Long id){
        Queue queue = getQueueByTwoId(principal,id);
        queueRepository.delete(queue);
        Product product = productRepository.findByID(id);
        try{
            product.setNumberOfPeople(product.getNumberOfPeople()-1);
        }
        catch (Exception e){
            throw new NullPointerException("Продукт не найден");
        }
        Queue nextQueue = queueRepository.findByProductIdAndActiveFalseOrderByDateOfCreated(id).get(0);
        if(nextQueue!=null) {
            nextQueue.setActive(true);
            queueRepository.save(nextQueue);
        }
        changePeopleAhead(id,0);
        productRepository.save(product);
        return product;
    }

    // уход пользователя из очереди
    public Product toGetOutLine(Long id, Principal principal){
        User user = getUserByPrincipal(principal);
        Queue queue = queueRepository.findByProductIdAndUserId(id ,user.getId());
        Integer escape = queue.getPeopleAhead();
        queueRepository.delete(queue);
        Product product = productRepository.findByID(id);
        product.setNumberOfPeople(product.getNumberOfPeople()-1);
        changePeopleAhead(id,escape);
        productRepository.save(product);
        return product;
    }

    //изменение количества людей впереди для пользователя при уходе впереди стоящего из очереди
    public void changePeopleAhead(Long productId, Integer escape){
        List<Queue> queueList = queueRepository.findByProductIdOrderByDateOfCreated(productId);
        int i = 0;
        for(Queue queue:queueList){
            if(i>=escape) {
                queue.setPeopleAhead(queue.getPeopleAhead() - 1);
                queueRepository.save(queue);
            }
            i++;
        }
    }

    // отдает экземпляр таблицы QUEUE
    public Queue getQueueByTwoId(Principal principal, Long productId) {
        Queue queue = null;
        try{
            queue = queueRepository.findByProductIdAndUserId(productId,getUserByPrincipal(principal).getId());
        }
        catch (Exception e){
            throw new NullPointerException("Очереди с product_id = "+productId+" и пользователем с id = " + principal.getName()+" нет");
        }
        return queue;
    }

    // отдает пользователя
    public User getUserByPrincipal(Principal principal) {
        if (principal == null) return new User();
        return userRepository.findByEmail(principal.getName());
    }
}
