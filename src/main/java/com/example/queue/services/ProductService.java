package com.example.queue.services;

import com.example.queue.models.Image;
import com.example.queue.models.Product;
import com.example.queue.models.Queue;
import com.example.queue.repositories.ImageRepository;
import com.example.queue.repositories.ProductRepository;
import com.example.queue.repositories.QueueRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final QueueRepository queueRepository;
    private final ImageRepository imageRepository;

    // список всех товаров
    public List<Product> getProductList() {
        List<Product> products = productRepository.findAll();
        return products;
    }

    // создание нового товара
    public Product saveProduct(Product product, MultipartFile file) throws IOException {
        if (product==null || product.getName()==null || product.getPrice()<0 || product.getNumber()<0){
            return null;
        }
        Image image = null;
        if(file.getSize()!=0){
            image = toImageEntity(file,product);
            product.setImage(image);
        }
        product.setNumberOfPeople(0);
        Product productFromDb = productRepository.save(product);
        image.setId(productFromDb.getImage().getId());
        productFromDb.setImageID(productFromDb.getImage().getId());
        return productRepository.save(product);
    }

    // передает новую картинку
    private Image toImageEntity(MultipartFile file,Product product) throws IOException {
        Image image = new Image();
        image.setName(file.getName());
        image.setOriginalFileName(file.getOriginalFilename());
        image.setContentType(file.getContentType());
        image.setSize(file.getSize());
        image.setBytes(file.getBytes());
        return image;
    }

    // изменение данных о товаре
    public Product changeProduct(Long id, String name, String description, Integer price, Integer number){
        Product product = productRepository.findByID(id);
        return productRepository.save(changeProductInfo(product, id, name, description, price, number));
    }

    public Product changeProductInfo(Product product,Long id, String name, String description, Integer price, Integer number){
        if(name!=null){
            product.setName(name);
        }
        if(description!=null){
            product.setDescription(description);
        }
        if(price!=null && price>0){
            product.setPrice(price);
        }
        if(number!=null && number>=0){
            product.setNumber(number);
            List<Queue> queueList = queueRepository.findByProductIdOrderByDateOfCreated(id);
            int i = 0;
            for(Queue queue:queueList){
                if(i>=number){
                    break;
                }
                queue.setActive(true);
                i++;
            }
        }
        return product;
    }

    // удаление товара
    public void deleteProduct(Long id){
        imageRepository.deleteById(productRepository.getById(id).getImageID());
        productRepository.deleteById(id);
    }

    // передает товар
    public Product getProductById(Long id) {
        return productRepository.getById(id);
    }
}
