package com.example.queue.services;

import com.example.queue.models.User;
import com.example.queue.models.enums.Role;
import com.example.queue.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository ;
    private final PasswordEncoder passwordEncoder;

    // создание пользователя или админа
    public boolean createUser(User user,Boolean admin,Role role){
        if(userRepository.findByEmail(user.getEmail())!=null){
            return false;
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setAdmin(admin);
        user.getRoles().add(role);
        userRepository.save(user);
        return true;
    }

    // находит всех админов
    public List<User> getAdmins(){
        List<User> admins = userRepository.findByAdminTrue();
        return admins;
    }
}
