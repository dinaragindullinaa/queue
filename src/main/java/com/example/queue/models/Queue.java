package com.example.queue.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Entity
@Table(name = "products_list")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Queue {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long userId;
    private Long productId;
    private LocalDateTime dateOfCreated;
    private Boolean active = false;
    private Integer peopleAhead;


    @PrePersist
    private void init() {
        dateOfCreated = LocalDateTime.now();
    }

}
