package com.example.queue.conrollers;

import com.example.queue.models.Product;
import com.example.queue.services.ProductService;
import com.example.queue.services.QueueService;
import com.example.queue.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class PagesController {

    private final ProductService productService;
    private final QueueService queueService;
    private final UserService userService;

    @GetMapping("/")
    public String mainPage(Principal principal, Model model){
        model.addAttribute(queueService.getUserByPrincipal(principal));
        model.addAttribute("admins",userService.getAdmins());
        return "mainPage";
    }

    //
    // список товаров
    @GetMapping("/products")
    public String products(Model model,Principal principal){
        model.addAttribute("products",productService.getProductList());
        model.addAttribute(queueService.getUserByPrincipal(principal));
        return "products";
    }

    //инфа о товаре по айдишнику
    @GetMapping("/products/{id}")
    public String productInfo(@PathVariable Long id, Model model,Principal principal) {
        model.addAttribute(queueService.getUserByPrincipal(principal));
        if(queueService.getQueueByTwoId(principal,id)!=null) {
            model.addAttribute("queue", queueService.getQueueByTwoId(principal, id));
        }
        Product product = productService.getProductById(id);
        model.addAttribute("product", product);
        model.addAttribute("img",productService.getProductById(id).getImageID());
        return "product-info";
    }

    // встать в очередь
    @PostMapping("/product/togetinline/{id}")
    public String toGetInLine(@PathVariable Long id, Principal principal, Model model){
        queueService.toGetInLine(id,principal);
        model.addAttribute(queueService.getUserByPrincipal(principal));
        return "redirect:/products/"+id;
    }

    // выйти из очереди
    @PostMapping("/product/togetoutline/{id}")
    public String toGetOutLine(@PathVariable Long id, Principal principal, Model model){
        queueService.toGetOutLine(id,principal);
        model.addAttribute(queueService.getUserByPrincipal(principal));
        return "redirect:/products/"+id;
    }

    // согласие на получение товара
    @PostMapping("/agree/{id}")
    public String agree(Principal principal,Model model, @PathVariable Long id) {
        queueService.takeProduct(principal,id);
        model.addAttribute(queueService.getUserByPrincipal(principal));
        return getForm(principal, model, id);
    }

    // отказ на получение товара
    @PostMapping("/disagree/{id}")
    public String disagree(Principal principal,Model model, @PathVariable Long id) {
        queueService.doNotTakeProduct(principal,id);
        model.addAttribute("user",queueService.getUserByPrincipal(principal));
        return getForm(principal, model, id);
    }

    // метод, формирующий данные страницы при отказе или согласии на получение товара
    private String getForm(Principal principal, Model model, @PathVariable Long id) {
        if(queueService.getQueueByTwoId(principal,id)!=null) {
            model.addAttribute("queue", queueService.getQueueByTwoId(principal, id));
        }
        model.addAttribute("img",productService.getProductById(id).getImageID());
        Product product =  productService.getProductById(id);
        model.addAttribute("product",product);
        return "redirect:/products/"+id;
    }


}
