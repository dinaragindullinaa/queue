package com.example.queue.conrollers;



import com.example.queue.models.User;
import com.example.queue.services.QueueService;
import com.example.queue.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

import static com.example.queue.models.enums.Role.ROLE_USER;

@Controller
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final QueueService queueService;

    // передается форма авторизации
    @GetMapping("/login")
    public String login(Principal principal, Model model) {
        model.addAttribute(queueService.getUserByPrincipal(principal));
        return "login";
    }

    // передается форма регистрации
    @GetMapping("/registration")
    public String registration(Principal principal, Model model) {
        model.addAttribute(queueService.getUserByPrincipal(principal));
        return "registration";
    }

    // регистируем нового пользователя и переходим на авторизацию
    @PostMapping("/registration")
    public String createUser(User user, Model model) {
        if (!userService.createUser(user,false,ROLE_USER)) {
            model.addAttribute("errorMessage", "Пользователь с email: " + user.getEmail() + " уже существует");
            return "registration";
        }
        return "redirect:/login";
    }

}