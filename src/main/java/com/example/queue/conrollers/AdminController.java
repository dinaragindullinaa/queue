package com.example.queue.conrollers;

import com.example.queue.models.Product;
import com.example.queue.models.User;
import com.example.queue.services.ProductService;
import com.example.queue.services.QueueService;
import com.example.queue.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

import static com.example.queue.models.enums.Role.ROLE_ADMIN;

@Controller
//@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
public class AdminController {

    private final ProductService productService;
    private final QueueService queueService;
    private final UserService userService;

    public AdminController(ProductService productService, QueueService queueService, UserService userService) {
        this.productService = productService;
        this.queueService = queueService;
        this.userService = userService;
    }

    // получаем представление с формой на создание нового товара
    @GetMapping("/createNewProduct")
    public String createNewProduct(Model model,Principal principal){
        model.addAttribute(queueService.getUserByPrincipal(principal));
        return "createNewProduct";
    }

    // создаем новый товар
    @PostMapping("/product/create")
    public String createProduct(@RequestParam(value = "file",required = false) MultipartFile file, Product product, Principal principal, Model model) throws IOException {
        System.out.println("try to create");
        model.addAttribute(queueService.getUserByPrincipal(principal));
        Product product1 = productService.saveProduct(product,file);
        if (product1==null){
            return "/createNewProduct";
        }
        return "redirect:/products/"+product.getID();//перезагружаем страницу
    }

    //получаем представление на изменение инфы о товаре
    @PostMapping("/change/{id}")
    public String change(Principal principal,Model model, @PathVariable Long id) {
        System.out.println("change 1");
        model.addAttribute(queueService.getUserByPrincipal(principal));
        Product product =  productService.getProductById(id);
        model.addAttribute("product",product);
        return "change";
    }

    // изменяем данные
    @PostMapping("/product/change/{id}")
    public String changeProduct(@PathVariable Long id,
                                @RequestParam(value = "name", required = false) String name,
                                @RequestParam(value = "description", required = false) String description,
                                @RequestParam(value = "price", required = false) Integer price,@RequestParam(value = "number", required = false) Integer number ) {
        productService.changeProduct(id,name,description,price,number);
        return "redirect:/products/"+id;
    }

    //удаление товара
    @PostMapping("/product/delete/{id}")
    public String deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);
        return "redirect:/products";
    }

    // передается представление со всеми админами
    @GetMapping("/admins")
    public String admins(Principal principal, Model model){
        model.addAttribute(queueService.getUserByPrincipal(principal));
        model.addAttribute("admins",userService.getAdmins());
        return "admins";
    }

    @GetMapping("/queue_info")
    public String queueInfo(Principal principal, Model model){
        model.addAttribute(queueService.getUserByPrincipal(principal));
        model.addAttribute("products", productService.getProductList());
        model.addAttribute("admins",userService.getAdmins());
        return "queue_info";
    }

    // передается форма на создание нового админа
    @GetMapping("/addAdmin")
    public String addAdmin(Principal principal, Model model) {
        model.addAttribute(queueService.getUserByPrincipal(principal));
        return "addAdmin";
    }

    // создание нового админа
    @PostMapping("/addAdmin")
    public String createAdmin(User user, Model model) {
        if (!userService.createUser(user, true,ROLE_ADMIN)) {
            model.addAttribute("errorMessage", "Пользователь с email: " + user.getEmail() + " уже существует");
            return "/addAdmin";
        }
        return "redirect:/admins";
    }
}

