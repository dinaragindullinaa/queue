package com.example.queue.repositories;

import com.example.queue.models.Queue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QueueRepository extends JpaRepository<Queue,Long> {
    Queue findByProductIdAndUserId(Long productId, Long userId);
    List<Queue> findByProductIdOrderByDateOfCreated(Long productId);
    List<Queue> findByProductIdAndActiveFalseOrderByDateOfCreated(Long productId);
    Integer countByProductId(Long productId);

}
